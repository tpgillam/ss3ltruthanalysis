default: Makefile cmt/Makefile.RootCore
	cd $(ROOTCOREDIR)/..; $(ROOTCOREDIR)/scripts/compile.sh; cd -

this: Makefile cmt/Makefile.RootCore
	cd cmt; make -f Makefile.RootCore; cd -

build: Makefile cmt/Makefile.RootCore
	cd $(ROOTCOREDIR)/..; $(ROOTCOREDIR)/scripts/build.sh $(ROOTCOREDIR)/../SS3LTruthAnalysis/share/packages.txt; cd -

clean: Makefile cmt/Makefile.RootCore
	cd $(ROOTCOREDIR)/..; $(ROOTCOREDIR)/scripts/clean.sh; cd -

cleanThis: Makefile cmt/Makefile.RootCore
	rm -rf bin obj StandAlone; $(ROOTCOREDIR)/scripts/get_field.sh cmt/Makefile.RootCore PACKAGE_CLEAN
