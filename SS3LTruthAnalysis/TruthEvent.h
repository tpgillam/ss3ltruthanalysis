#ifndef TRUTHEVENT_H
#define TRUTHEVENT_H

#include "SS3LTruthAnalysis/AnalysisClass.h"
#include <cmath>

namespace SS3LTruthAnalysis {
  class TruthEvent : AnalysisEvent
  {
    public:
      TruthEvent(double Ex, double Ey) :  _met(Ex,Ey,0,sqrt(Ex*Ex+Ey*Ey),0,MET,0) {};
      virtual AnalysisObjects getElectrons(float ptCut,float etaCut,int isolation) { return filterObjects( _baseElectrons, ptCut, etaCut, isolation); };
      virtual AnalysisObjects getMuons(float ptCut,float etaCut,int isolation)  { return filterObjects( _baseMuons, ptCut, etaCut, isolation); };
      virtual AnalysisObjects getTaus(float ptCut,float etaCut,int isolation) { return filterObjects( _baseTaus, ptCut, etaCut, isolation); };
      virtual AnalysisObjects getJets(float ptCut,float etaCut,int btag) { return filterObjects( _baseJets, ptCut, etaCut, btag); };
      virtual AnalysisObject  getMET() { return _met; };
      void addElectron(double Px, double Py, double Pz, double E, int iso, int idx, int charge) { 
        _baseElectrons.push_back(AnalysisObject(Px,Py,Pz,E,iso,ELECTRON,idx,charge));
      };
      void addElectron(TLorentzVector tlv, int iso, int idx, int charge) { 
        _baseElectrons.push_back(AnalysisObject(tlv,iso,ELECTRON,idx,charge));
      };
      void addMuon(double Px, double Py, double Pz, double E, int iso, int idx, int charge) { 
        _baseMuons.push_back(AnalysisObject(Px,Py,Pz,E,iso,MUON,idx,charge));
      };
      void addMuon(TLorentzVector tlv, int iso, int idx, int charge) { 
        _baseMuons.push_back(AnalysisObject(tlv,iso,MUON,idx,charge));
      };
      void addTau(double Px, double Py, double Pz, double E, int iso, int idx, int charge) { 
        _baseTaus.push_back(AnalysisObject(Px,Py,Pz,E,iso,TAU,idx,charge));
      };
      void addTau(TLorentzVector tlv, int iso, int idx, int charge) { 
        _baseTaus.push_back(AnalysisObject(tlv,iso,TAU,idx,charge));
      };
      void addJet(double Px, double Py, double Pz, double E, int iso, int idx) { 
        _baseJets.push_back(AnalysisObject(Px,Py,Pz,E,iso,JET,idx));
      };
      void addJet(TLorentzVector tlv, int iso, int idx) { 
        _baseJets.push_back(AnalysisObject(tlv,iso,JET,idx));
      };
    private:
      AnalysisObjects _baseElectrons;
      AnalysisObjects _baseMuons;
      AnalysisObjects _baseTaus;
      AnalysisObjects _baseJets;
      AnalysisObject _met;
  };
}

#endif
