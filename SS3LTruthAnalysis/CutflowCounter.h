#ifndef CUTFLOWCOUNTER_H
#define CUTFLOWCOUNTER_H
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace SS3LTruthAnalysis {
  class CutflowCounter {
    public:
      CutflowCounter() : _weight(1), _lastIndex(-1) { addEntry("All"); };
      int addEntry(const char *label,int num=0);
      void pass(int num,double weight=1);
      void pass(const char *label,double weight=1);
      //the following should only be called once per event and before the pass method is called
      void setEventWeight(double weight=1.) { _weight=weight; pass(0); };
      void saveCutflow(const char *filename);

    private:
      double _weight;
      int _lastIndex;
      std::map<std::string,int> label2idx;
      std::map<int,std::string> idx2label;
      std::map<int,double> unweightedSums;
      std::map<int,double> weightedSums;
      std::map<int,double> weightedSquaredSums;

  };
}
#endif
