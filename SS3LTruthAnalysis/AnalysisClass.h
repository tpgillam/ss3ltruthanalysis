#ifndef ANALYSISCLASS_H
#define ANALYSISCLASS_H

#include "SS3LTruthAnalysis/CutflowCounter.h"
#include "TLorentzVector.h"
#include <vector>
#include <algorithm>

namespace SS3LTruthAnalysis {
  enum AnalysisObjectType { ELECTRON,MUON,TAU,JET,MET,COMBINED};

  class AnalysisObject : public TLorentzVector
  {
    public:
      AnalysisObject(double Px, double Py, double Pz, double E, 
          int iso, AnalysisObjectType type, int orgIndex, int charge=0) :
        TLorentzVector(Px,Py,Pz,E), _iso(iso), _type(type), _orgIndex(orgIndex), _charge(charge) {};
      AnalysisObject(TLorentzVector tlv,
          int iso, AnalysisObjectType type, int orgIndex, int charge=0) :
        TLorentzVector(tlv), _iso(iso), _type(type), _orgIndex(orgIndex), _charge(charge) {};
      virtual bool isIsolated(int isoCut=1) const { return _iso>=isoCut; }
      virtual bool isBtagged(int btagCut=1) const { return _iso>=btagCut; }
      AnalysisObjectType type() const { return _type; }
      int charge() const { return _charge; }

    private:
      int _iso; //used for btag in case of jets
      AnalysisObjectType _type;
      int _orgIndex;
      int _charge;
  };

  typedef std::vector<AnalysisObject> AnalysisObjects;

  class AnalysisEvent
  {
    public:
      virtual AnalysisObjects getElectrons(float ptCut, float modEtaCut, int isolation=1)=0;
      virtual AnalysisObjects getMuons(float ptCut, float modEtaCut, int isolation=1)=0;
      virtual AnalysisObjects getTaus(float ptCut, float modEtaCut, int isolation=1)=0;
      virtual AnalysisObjects getJets(float ptCut, float modEtaCut, int btag=0)=0;
      virtual AnalysisObject  getMET()=0;
      virtual ~AnalysisEvent() {};
  };

  class AnalysisClass
  {
    public:
      virtual void Init(CutflowCounter *cutflow) = 0;
      virtual void ProcessEvent(CutflowCounter *cutflow, AnalysisEvent *event) = 0;
      virtual void Final(CutflowCounter *cutflow) = 0;
      virtual ~AnalysisClass() {};
  };

  AnalysisObjects overlapRemoval(AnalysisObjects cands,
      AnalysisObjects others,
      float deltaR);

  AnalysisObjects filterObjects(AnalysisObjects cands,
      float ptCut,float modEtaCut=100., int isolation=0); 

  int countObjects(AnalysisObjects cands,
      float ptCut,float modEtaCut=100., int isolation=0);

}

#endif
