#ifndef SS3L_H
#define SS3L_H

#include "SS3LTruthAnalysis/AnalysisClass.h"

namespace SS3LTruthAnalysis {
  class SS3L : public AnalysisClass {
    public:
      void Init(CutflowCounter *cutflow);
      void ProcessEvent(CutflowCounter *cutflow, AnalysisEvent *event);
      void Final(CutflowCounter *cutflow);
  };
}

#endif // SS3L_H
