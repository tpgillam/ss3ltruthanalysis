#define TruthAnalysis_cxx
// The class definition in TruthAnalysis.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// Root > T->Process("TruthAnalysis.C")
// Root > T->Process("TruthAnalysis.C","some options")
// Root > T->Process("TruthAnalysis.C+")
//

#include <cmath>

#include "SS3LTruthAnalysis/TruthAnalysis.h"
#include "SS3LTruthAnalysis/TruthEvent.h"

namespace SS3LTruthAnalysis {
  void TruthAnalysis::Begin(TTree * /*tree*/)
  {
    // The Begin() function is called at the start of the query.
    // When running with PROOF Begin() is only called on the client.
    // The tree argument is deprecated (on PROOF 0 is passed).

    TString option = GetOption();

    for(const auto& analysis : analysisList) {
      CutflowCounter* cutflow=cutflowList[analysis.first];
      analysis.second->Init(cutflow);
    }

  }

  void TruthAnalysis::SlaveBegin(TTree * /*tree*/)
  {
    // The SlaveBegin() function is called after the Begin() function.
    // When running with PROOF SlaveBegin() is called on each slave server.
    // The tree argument is deprecated (on PROOF 0 is passed).

    TString option = GetOption();

  }

  Bool_t TruthAnalysis::Process(Long64_t entry)
  {
    // The Process() function is called for each entry in the tree (or possibly
    // keyed object in the case of PROOF) to be processed. The entry argument
    // specifies which entry in the currently loaded tree is to be processed.
    // It can be passed to either TruthAnalysis::GetEntry() or TBranch::GetEntry()
    // to read either all or the required parts of the data. When processing
    // keyed objects with PROOF, the object is already loaded and is available
    // via the fObject pointer.
    //
    // This function should contain the "body" of the analysis. It can contain
    // simple or elaborate selection criteria, run algorithms on the data
    // of the event and typically fill histograms.
    //
    // The processing can be stopped by calling Abort().
    //
    // Use fStatus to set the return value of TTree::Process().
    //
    // The return value is currently not used.
    if ((entry%10000)==0) std::cout<<"At event "<<entry<<std::endl;
    GetEntry(entry);

    TruthEvent* event=new TruthEvent(MET_Truth_NonInt_etx/1000.,MET_Truth_NonInt_ety/1000.);

    TLorentzVector tlv(0.,0.,0.,0.);
    for(int idx=0; idx<el_n; idx++) {
      int pdgID=0;
      if (el_parent_index->at(idx).size()>0) pdgID=fabs(mc_pdgId->at(el_parent_index->at(idx)[0]));
      int iso=(pdgID==25 || pdgID==24 || pdgID==23 || pdgID==15 || (pdgID>1000000 && pdgID<3000000));

      tlv.SetPtEtaPhiM(el_pt->at(idx)/1000.,el_eta->at(idx),el_phi->at(idx),el_m->at(idx)/1000.);
      int charge = (int)(el_charge->at(idx));
      event->addElectron(tlv,iso,idx,charge);
    }
    for(int idx=0; idx<mu_n; idx++) {
      int pdgID=0;
      if (mu_parent_index->at(idx).size()>0) pdgID=fabs(mc_pdgId->at(mu_parent_index->at(idx)[0]));
      int iso=(pdgID==25 || pdgID==24 || pdgID==23 || pdgID==15 || (pdgID>1000000 && pdgID<3000000));

      tlv.SetPtEtaPhiM(mu_pt->at(idx)/1000.,mu_eta->at(idx),mu_phi->at(idx),mu_m->at(idx)/1000.);
      int charge = (int)(mu_charge->at(idx));
      event->addMuon(tlv,iso,idx,charge);
    }
    for(int idx=0; idx<tau_n; idx++) {
      int pdgID=0;
      if (tau_parent_index->at(idx).size()>0) pdgID=fabs(mc_pdgId->at(tau_parent_index->at(idx)[0]));
      int iso=(pdgID==25 || pdgID==24 || pdgID==23 || pdgID==15 || (pdgID>1000000 && pdgID<3000000));

      tlv.SetPtEtaPhiM(tau_pt->at(idx)/1000.,tau_eta->at(idx),tau_phi->at(idx),tau_m->at(idx)/1000.);
      int charge = (int)(tau_charge->at(idx));
      event->addTau(tlv,iso,idx,charge);
    }
    for(int idx=0; idx<jet_AntiKt4TruthJets_n; idx++) {
      tlv.SetPtEtaPhiM(jet_AntiKt4TruthJets_pt->at(idx)/1000.,jet_AntiKt4TruthJets_eta->at(idx),jet_AntiKt4TruthJets_phi->at(idx),jet_AntiKt4TruthJets_m->at(idx)/1000.);
      event->addJet(tlv,abs(jet_AntiKt4TruthJets_flavor_partonFlavor->at(idx))==5,idx);
    }

    //calculate event weight here
    double weight=1;
    for(const auto& analysis : analysisList) {
      CutflowCounter* cutflow=cutflowList[analysis.first];
      cutflow->setEventWeight(weight);
      analysis.second->ProcessEvent(cutflow,(AnalysisEvent *) event);
    }

    delete event;
    return kTRUE;
  }

  void TruthAnalysis::SlaveTerminate()
  {
    // The SlaveTerminate() function is called after all entries or objects
    // have been processed. When running with PROOF SlaveTerminate() is called
    // on each slave server.

  }

  void TruthAnalysis::Terminate()
  {
    // The Terminate() function is the last function to be called during
    // a query. It always runs on the client, it can be used to present
    // the results graphically or save the results to file.
    for(const auto& analysis : analysisList) {
      CutflowCounter* cutflow=cutflowList[analysis.first];
      analysis.second->Final(cutflow);
      cutflow->saveCutflow((analysis.first).c_str());    
    }  
  }
}
