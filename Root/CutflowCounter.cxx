#include <stdlib.h>

#include "SS3LTruthAnalysis/CutflowCounter.h"

namespace SS3LTruthAnalysis {
  int CutflowCounter::addEntry(const char *label,int num) {
    std::string name(label);
    if (num==0) num=_lastIndex+1;
    if (label2idx.find(name)!=label2idx.end()) {
      std::cerr<<"Duplicate signal region label: "<<name<<std::endl;
      exit(1);
    }
    if (idx2label.find(num)!=idx2label.end()) {
      std::cerr<<"Duplicate signal region number: "<<num<<std::endl;
      exit(1);
    }
    _lastIndex=num;
    label2idx[name]=num;
    idx2label[num]=name;
    unweightedSums[num]=0.;
    weightedSums[num]=0.;
    weightedSquaredSums[num]=0.;

    return num;
  }

  void CutflowCounter::pass(int num,double weight) {
    if (idx2label.find(num)==idx2label.end()) {
      std::cerr<<"Unknown signal region number: "<<num<<std::endl;
      exit(1);
    }
    weight*=_weight;
    unweightedSums[num]+=1;
    weightedSums[num]+=weight;
    weightedSquaredSums[num]+=weight*weight;
  }

  void CutflowCounter::pass(const char *label,double weight) {
    std::string name(label);
    if (label2idx.find(name)==label2idx.end()) {
      std::cerr<<"Unknown signal region label: "<<name<<std::endl;
      exit(1);
    }
    pass(label2idx[name],weight);
  }

  void CutflowCounter::saveCutflow(const char *filename) {
    std::ofstream csv_ofile;
    csv_ofile.open(filename);
    csv_ofile << "SR,Acceptance,Sum,WeightedSum,WeightedSquareSum"<<std::endl;
    for(const auto& idx: idx2label) {
      csv_ofile<< idx.second<<","
        <<unweightedSums[idx.first]/unweightedSums[0]<<","
        <<unweightedSums[idx.first]<<","
        <<weightedSums[idx.first]<<","
        <<weightedSquaredSums[idx.first]<<std::endl;
    }

  }
}
