#include "SS3LTruthAnalysis/TruthAnalysis.h"
#include "SS3LTruthAnalysis/SS3L.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class std::vector<std::vector<float> >;
#pragma link C++ class std::vector<std::vector<unsigned int> >;
#pragma link C++ class std::vector<std::vector<std::pair<std::string, bool> > >+;
#pragma link C++ class std::vector<std::pair<std::string, bool> >+;
#pragma link C++ class std::pair<std::string, bool>+;
#pragma link C++ class std::pair<std::string, std::string>+;

#pragma link C++ class SS3LTruthAnalysis::TruthAnalysis+;
#pragma link C++ class SS3LTruthAnalysis::SS3L+;

#endif
