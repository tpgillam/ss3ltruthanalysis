#include "SS3LTruthAnalysis/AnalysisClass.h"

namespace SS3LTruthAnalysis {
  AnalysisObjects overlapRemoval(AnalysisObjects cands,
      AnalysisObjects others,
      float deltaR) {
    AnalysisObjects reducedList;

    for (const auto &cand : cands) {
      bool overlap=false;
      for (const auto &other : others) {
        if (cand.DeltaR(other)<deltaR) {
          overlap=true;
          break;
        }
      }
      if (!overlap) reducedList.push_back(cand);
    }
    return reducedList;
  }

  AnalysisObjects filterObjects(AnalysisObjects cands,
      float ptCut,float modEtaCut,int isolation) {
    AnalysisObjects reducedList;
    for(const auto& cand : cands) {
      if ((cand.Pt()>=ptCut) &&
          (fabs(cand.Eta())<modEtaCut) &&
          (cand.isIsolated(isolation))) reducedList.push_back(cand);
    }
    return reducedList;
  }

  int countObjects(AnalysisObjects cands,
      float ptCut,float modEtaCut,int isolation) {
    int count=0;
    for(const auto& cand : cands) {
      if ((cand.Pt()>=ptCut) &&
          (fabs(cand.Eta())<modEtaCut) &&
          (cand.isIsolated(isolation))) count++;
    }
    return count;
  }
}
