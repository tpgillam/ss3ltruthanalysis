#include "SS3LTruthAnalysis/SS3L.h"

namespace SS3LTruthAnalysis {

  void SS3L::Init(CutflowCounter *cutflow)
  {
    cutflow->addEntry("2lep10GeV");
    cutflow->addEntry("2lep15GeV");
    cutflow->addEntry("mll12GeV");
    cutflow->addEntry("sameSign3L");
    cutflow->addEntry("lep0Pt20GeV");

    cutflow->addEntry("SR0b");
    cutflow->addEntry("SR1b");
    cutflow->addEntry("SR3b");
    cutflow->addEntry("SR3Llow");
    cutflow->addEntry("SR3Lhigh");
    cutflow->addEntry("SR0b_disc");
    cutflow->addEntry("SR1b_disc");
    cutflow->addEntry("SR3b_disc");
    cutflow->addEntry("SR3Llow_disc");
    cutflow->addEntry("SR3Lhigh_disc");
  }

  void SS3L::ProcessEvent(CutflowCounter *cutflow, AnalysisEvent *event)
  {
    auto baselineElectrons  = event->getElectrons(10, 2.47);
    // TODO: check if it's better to use muons with |eta|<2.4
    auto baselineMuons      = event->getMuons(10, 2.5);
    auto jets_eta28 = event->getJets(20., 2.8);
    auto metVec     = event->getMET();

    // Standard SUSY overlap removal      
    jets_eta28         = overlapRemoval(jets_eta28, baselineElectrons, 0.2);
    baselineElectrons  = overlapRemoval(baselineElectrons, jets_eta28, 0.4);
    baselineMuons      = overlapRemoval(baselineMuons, jets_eta28, 0.4);
    baselineElectrons  = overlapRemoval(baselineElectrons, baselineMuons, 0.1);

    if ((baselineElectrons.size() + baselineMuons.size()) > 1) {
      cutflow->pass("2lep10GeV");
    } else { return; }

    auto signalElectrons = filterObjects(baselineElectrons, 15.);
    auto signalMuons = filterObjects(baselineMuons, 15.);
    
    AnalysisObjects signalLeptons;
    signalLeptons.insert(signalLeptons.end(), signalElectrons.begin(), signalElectrons.end());
    signalLeptons.insert(signalLeptons.end(), signalMuons.begin(), signalMuons.end());

    int nLep15 = signalLeptons.size();
    if (nLep15 > 1) {
      cutflow->pass("2lep15GeV");
    } else { return; }

    auto lep0 = signalLeptons[0];
    auto lep1 = signalLeptons[1];

    double lep2Pt = 0.;
    if (nLep15 > 2) {
      auto lep2 = signalLeptons[2];
      lep2Pt = lep2.Pt();
    }

    if ((lep0 + lep1).Mag() > 12.) {
      cutflow->pass("mll12GeV");
    } else { return; }

    if ((lep0.charge() == lep1.charge()) || nLep15 > 2) {
      cutflow->pass("sameSign3L");
    } else { return; }

    if (lep0.Pt() > 20.) {
      cutflow->pass("lep0Pt20GeV");
    } else { return; }

    // Variables we'll be cutting on
    double met = metVec.Et();
    int nJets40 = countObjects(jets_eta28, 40., 2.8);
    int nBJets20 = countObjects(jets_eta28, 20., 2.5, true);
    double mt = sqrt(2 * lep0.Pt() * met * (1 - cos(lep0.Phi() - atan2(metVec.Y(), metVec.X()))));

    double meff = 0.;
    for (const auto &jet : jets_eta28) {
      if (jet.Pt() > 40.) meff += jet.Pt();
    }
    meff += met;
    for (const auto &lep : signalLeptons) {
      meff += lep.Pt();
    }
    double mllSFOS1 = -1.;
    double mllSFOS2 = -1.;

    bool foundOneSFOS = false;
    if (lep0.type() == lep1.type() && lep0.charge() != lep1.charge()) {
      mllSFOS1 = (lep0 + lep1).Mag();
      foundOneSFOS = true;
    }
    if (nLep15 > 2) {
      auto lep2 = signalLeptons[2];
      if (lep0.type() == lep2.type() && lep0.charge() != lep2.charge()) {
        if (foundOneSFOS) {
          mllSFOS2 = (lep0 + lep2).Mag();
        } else {
          mllSFOS1 = (lep0 + lep2).Mag();
        }
        foundOneSFOS = true;
      }
      if (lep1.type() == lep2.type() && lep1.charge() != lep2.charge()) {
        if (foundOneSFOS) {
          mllSFOS2 = (lep1 + lep2).Mag();
        } else {
          mllSFOS1 = (lep1 + lep2).Mag();
        }
        foundOneSFOS = true;
      }
    }


    bool passSR3b     = ( nLep15>=2 && nJets40>= 5 && nBJets20>=3 );
    bool passSR0b     = ( nLep15==2 && met>150 && nJets40>=3 && nBJets20==0 && mt>100 );
    bool passSR1b     = ( nLep15==2 && met>150 && nJets40>=3 && nBJets20>=1 && mt>100 && !passSR3b ); 
    bool passSR3Llow  = ( nLep15>=3 && nJets40>=4 && met > 50 && met<150 && (mllSFOS1 < 84 || mllSFOS1 > 98) && (mllSFOS2 < 84|| mllSFOS2 > 98)  && !passSR3b );
    bool passSR3Lhigh = ( nLep15>=3 && nJets40>=4 && met>150 && !passSR3b );

    bool passSR3b_disc     = passSR3b     && meff > 350;
    bool passSR0b_disc     = passSR0b     && meff > 400;
    bool passSR1b_disc     = passSR1b     && meff > 700;
    bool passSR3Llow_disc  = passSR3Llow  && meff > 400;
    bool passSR3Lhigh_disc = passSR3Lhigh && meff > 400;


    if (passSR0b) cutflow->pass("SR0b");
    if (passSR1b) cutflow->pass("SR1b");
    if (passSR3b) cutflow->pass("SR3b");
    if (passSR3Llow) cutflow->pass("SR3Llow");
    if (passSR3Lhigh) cutflow->pass("SR3Lhigh");

    if (passSR0b_disc) cutflow->pass("SR0b_disc");
    if (passSR1b_disc) cutflow->pass("SR1b_disc");
    if (passSR3b_disc) cutflow->pass("SR3b_disc");
    if (passSR3Llow_disc) cutflow->pass("SR3Llow_disc");
    if (passSR3Lhigh_disc) cutflow->pass("SR3Lhigh_disc");
  }

  void SS3L::Final(CutflowCounter *)//cutflow)
  {
  }

}
