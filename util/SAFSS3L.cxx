#include <iostream>

#include <TFile.h>
#include <TChain.h>

#include "SS3LTruthAnalysis/TruthAnalysis.h"
#include "SS3LTruthAnalysis/SS3L.h"

int main(int argc, char **argv) {
  if (argc<3) {
    std::cout << "usage: " << argv[0] << " <name> <rootfile1> [rootfile2] ..." << std::endl;
    return 1;
  }
  
  TChain *tree = new TChain("truth");
  for (unsigned int i = 2; i < (unsigned int)argc; ++i) {
    if (tree->Add(argv[i]) < 1) {
      std::cout<<"failed to find proper ntuple in: " << argv[i] << std::endl;
      return 2;
    }
    std::cout<<"Processing: " << argv[i] << std::endl;
  }

  SS3LTruthAnalysis::TruthAnalysis* selector = new SS3LTruthAnalysis::TruthAnalysis(tree);
  selector->addAnalysis(argv[1], new SS3LTruthAnalysis::SS3L());

  tree->Process(selector);

  return 0;
}
